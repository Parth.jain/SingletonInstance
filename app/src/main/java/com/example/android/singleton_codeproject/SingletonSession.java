package com.example.android.singleton_codeproject;

/**
 * Created by parth on 20/12/17.
 */
public class SingletonSession {

    private static SingletonSession instance;
    private String mUsername;
    //no outer class can initialize this class's object
    private SingletonSession() {

    }

    public static SingletonSession Instance()
    {
        //if no instance is initialized yet then create new instance
        //else return stored instance
        if (instance == null)
        {
            instance = new SingletonSession();
        }
        return instance;
    }


    public void setUsername(String username) {

        mUsername = username;
    }

    public String getUsername() {
        return mUsername;
    }
}